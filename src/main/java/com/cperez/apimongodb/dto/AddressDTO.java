package com.cperez.apimongodb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDTO {
    private String nameStreet;
    private String number;
    private String reference;
    private UbigeoDTO ubigeo;
}
