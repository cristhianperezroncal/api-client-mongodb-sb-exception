package com.cperez.apimongodb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientDTO {
    private String name;
    private String lastname;
    private String paymentMethod;
    private ArrayList<AddressDTO> addressDTOS;
    private ArrayList<ProductDTO> productDTOS;
}
