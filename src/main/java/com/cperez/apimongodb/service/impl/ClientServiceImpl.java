package com.cperez.apimongodb.service.impl;

import com.cperez.apimongodb.dto.ClientDTO;
import com.cperez.apimongodb.dto.ProductDTO;
import com.cperez.apimongodb.exception.CantidadNoAdmitida;
import com.cperez.apimongodb.exception.MetodoDePagoNoAdmitidoException;
import com.cperez.apimongodb.model.Client;
import com.cperez.apimongodb.repository.ClientRepository;
import com.cperez.apimongodb.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> getClients() {
        return clientRepository.getClients();
    }

    @Override
    public Client createdClient(ClientDTO clientDTO) {
        Client client = new Client(clientDTO);
        List<ProductDTO> productDTOS = clientDTO.getProductDTOS();
        double subTotal = 0;
        int quantity = 0;
        for (ProductDTO productDTO : productDTOS) {
            subTotal += productDTO.getPrice()* productDTO.getQuantity();
            quantity += productDTO.getQuantity();
        }

        if (quantity<10) {
            throw new CantidadNoAdmitida();
        }

        if (!"Efectivo".equals(clientDTO.getPaymentMethod()) && !"Visa".equals(clientDTO.getPaymentMethod()) && !"Mastercard".equals(clientDTO.getPaymentMethod())) {
            throw new MetodoDePagoNoAdmitidoException(clientDTO.getPaymentMethod());
        }

        double descuento = 0;
        if (clientDTO.getPaymentMethod().equals("Efectivo")) descuento = subTotal*0.20;
        if (clientDTO.getPaymentMethod().equals("Visa")) descuento = subTotal*0.10;
        if (clientDTO.getPaymentMethod().equals("Mastercard ")) descuento = 0;

        double total = subTotal-descuento;

        client.setTotal(total);

        return clientRepository.createdClient(client);
    }

    @Override
    public List<Client> getClientByName(String name) {
        return clientRepository.getClientByName(name);
    }

    @Override
    public List<Client> getClientByNameStreet(String nameStreet) {
        return clientRepository.getClientByNameStreet(nameStreet);
    }

    @Override
    public List<Client> getClientByTotal(double gtTotal) {
        return clientRepository.getClientByTotal(gtTotal);
    }
}
