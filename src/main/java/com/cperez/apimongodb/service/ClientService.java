package com.cperez.apimongodb.service;

import com.cperez.apimongodb.dto.ClientDTO;
import com.cperez.apimongodb.model.Client;

import java.util.List;

public interface ClientService {
    List<Client> getClients();
    Client createdClient(ClientDTO clientDTO);

    List<Client> getClientByName(String name);

    List<Client> getClientByNameStreet(String nameStreet);

    List<Client> getClientByTotal(double gtTotal);
}
