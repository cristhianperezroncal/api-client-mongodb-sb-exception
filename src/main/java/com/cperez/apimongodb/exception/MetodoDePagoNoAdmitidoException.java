package com.cperez.apimongodb.exception;

public class MetodoDePagoNoAdmitidoException extends RuntimeException{
    public MetodoDePagoNoAdmitidoException(String metodoDePago) {
        super("No se admite el método de pago " + metodoDePago);
    }
}
