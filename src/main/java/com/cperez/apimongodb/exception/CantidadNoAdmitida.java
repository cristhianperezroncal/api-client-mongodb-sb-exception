package com.cperez.apimongodb.exception;

public class CantidadNoAdmitida extends RuntimeException{
    public CantidadNoAdmitida( ) {
        super("No se pueden comprar menos de 10 productos");
    }
}
