package com.cperez.apimongodb.controller;

import com.cperez.apimongodb.dto.ClientDTO;
import com.cperez.apimongodb.errorresponse.ErrorResponse;
import com.cperez.apimongodb.model.Client;
import com.cperez.apimongodb.service.ClientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.pattern.PathPattern;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/clients")
@RequiredArgsConstructor
public class ClientController {
    @Autowired
    private ClientService clientService;


    @PostMapping
    public ResponseEntity<?> createdClient(@RequestBody ClientDTO clientDTO) {
        try {
            Client client = clientService.createdClient(clientDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(client);
        } catch (Exception ex) {
            ErrorResponse errorResponse = new ErrorResponse(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
            return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping
    public ResponseEntity<List<Client>> getClients() {
        try {
            List<Client> clients = clientService.getClients();
            return ResponseEntity.status(HttpStatus.OK).body(clients);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/byname/{name}")
    public ResponseEntity<List<Client>> getClientsByName(@PathVariable String name){
        try {
            List<Client> clients = clientService.getClientByName(name);
            return ResponseEntity.status(HttpStatus.OK).body(clients);
        }
        catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/bystreet/{nameStreet}")
    public ResponseEntity<List<Client>> getClientByNameStreet (@PathVariable String nameStreet){
        try {
            List<Client> clients = clientService.getClientByNameStreet(nameStreet);
            return ResponseEntity.status(HttpStatus.OK).body(clients);
        }
        catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/bytotal/{total}")
    public ResponseEntity<List<Client>> getClientByTotal (@PathVariable double total){
        try {
            List<Client> clients = clientService.getClientByTotal(total);
            return ResponseEntity.status(HttpStatus.OK).body(clients);
        }
        catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }



}
