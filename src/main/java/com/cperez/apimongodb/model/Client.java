package com.cperez.apimongodb.model;

import com.cperez.apimongodb.dto.AddressDTO;
import com.cperez.apimongodb.dto.ClientDTO;
import com.cperez.apimongodb.dto.ProductDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Client {
    @Id
    private String id;
    private String name;
    private String lastname;
    private String paymentMethod;
    private double total;
    private List<AddressDTO> addresses;
    private ArrayList<ProductDTO> products;

    public Client(ClientDTO clientDTO) {
        this.name = clientDTO.getName();
        this.lastname = clientDTO.getLastname();
        this.paymentMethod = clientDTO.getPaymentMethod();
        this.addresses = clientDTO.getAddressDTOS();
        this.products = clientDTO.getProductDTOS();
    }



}
