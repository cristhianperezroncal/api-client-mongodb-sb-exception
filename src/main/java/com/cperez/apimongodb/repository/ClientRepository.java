package com.cperez.apimongodb.repository;

import com.cperez.apimongodb.model.Client;

import java.util.List;

public interface ClientRepository {
    List<Client> getClients();
    Client createdClient(Client client);

    List<Client> getClientByName(String name);

    List<Client> getClientByNameStreet(String nameStreet);

    List<Client> getClientByTotal(double gtTotal);

}
